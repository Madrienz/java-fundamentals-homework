package org.madrien;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Calculator {
    private static Logger logger = LogManager.getLogger();

    public static int plus(int a, int b) {
        logger.trace("Entered plus() with values: " + a +" and " + b);
        int result = a+b;
        logger.trace("Exiting plus() with value: " + result);
        return result;
    }

    public static int minus(int a, int b) {
        logger.trace("Entered minus() with values: " + a +" and " + b);
        int result = a-b;
        logger.trace("Exiting minus() with value: " + result);
        return result;
    }

    public static long multiply(int a, int b) {
        logger.trace("Entered multiply() with values: " + a +" and " + b);
        long result = a*b;
        logger.trace("Exiting multiply() with value: " + result);
        return result;
    }

    public static double divide(int a, int b) {
        logger.trace("Entered divide() with values: " + a +" and " + b);
        if(b == 0) {
            logger.info("Illegal argument exception.");
            throw new IllegalArgumentException("Can not divide by zero.");
        }
        double result = (double) a / (double) b;
        logger.trace("Exiting divide() with value: " + result);
        return result;
    }

    public static double sqrt(int n) {
        logger.trace("Entered sqrt() with value: " + n);
        if(n < 0) {
            logger.info("Illegal argument exception.");
            throw new IllegalArgumentException("Can not calculate square root from negative number.");
        }
        double result = Math.sqrt(n);
        logger.trace("Exiting sqrt() with value: " + result);
        return result;
    }

    public static boolean isPrime(int n) {
        logger.trace("Entered isPrime() with value: " + n);
        if(n % 2 == 0) {
            logger.trace("Exiting isPrime() returning false. Even number.");
            return false;
        }
        for(int i = 3; i * i <= n; i += 2) {
            if(n % i == 0) {
                logger.trace("Exiting isPrime() returning false. Not a prime.");
                return false;
            }
        }
        logger.trace("Exiting isPrime() returning true.");
        return true;
    }

    public static int fibonacciNum(int n) {
        logger.trace("Entered fibonacciNum() with value: " + n);
        if(n < 0) {
            logger.info("Illegal argument exception.");
            throw new IllegalArgumentException("Argument should be positive.");
        }
        if(n == 0) {
            logger.trace("Exiting fibonacciNum() with value: " + 0);
            return 0;
        } else if(n == 1) {
            logger.trace("Exiting fibonacciNum() with value: " + 1);
            return 1;
        }
        return fibonacciNum(n - 1) + fibonacciNum(n - 2);
    }

    public static int summarizeNumberRow(int[] row) {
        logger.trace("Entered summarizeNumberRow() with array size of: " + row.length);
        if(row.length == 0) {
            logger.info("Illegal argument exception.");
            throw new IllegalArgumentException("The array is empty.");
        }
        int sum = 0;
        for(int i = 0; i < row.length; i++) {
            logger.trace("Array value at index " + i + ": " + row[i]);
            sum += row[i];
        }
        logger.trace("Exiting summarizeNumberRow() with value: " + sum);
        return sum;
    }
}
