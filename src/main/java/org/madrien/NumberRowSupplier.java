package org.madrien;

public class NumberRowSupplier {
    final int[] return123() {
        return new int[] {1,2,3};
    }

    static int[] return456() {
        return new int[] {4,5,6};
    }


    private int[] return789() {
        return new int[] {7,8,9};
    }

    public int[] callPrivateReturn789() {
        return return789();
    }

    public int[] returnArray() {
        return new int[] {10,11,12};
    }
}
